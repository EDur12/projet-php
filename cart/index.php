<html>
<head>
  <meta charset="UTF-8">
  <title>LebonCoinCoin</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet"/>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.css" rel="stylesheet"/>
  <link rel="icon" href="./assets/favicon.ico" type="image/svg+xml">
</head>

<body>
<?php
$isHomePage = false;
$isValidatePage = false;
include '../header.php';
session_start();
if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
}
require_once('../fonctions/navBar.php');
require_once('../fonctions/db_manager.php');
$db = new DB_manager("phpmyadmin", "adminphp", "totoduphp", "localhost");
if (isset($_GET['error'])) {
  echo '<div class="alert alert-danger" role="alert"> Not enough stock on this article </div>';
}
if ($db->get_user_carts($user->user_id) == null) {
  echo '<div class="d-flex justify-content-center">
  <div class="card text-center mt-5" style="width: 18rem;">
    <div class="card-body">
      <h5 class="card-title">Your cart is empty</h5>
      <p class="card-text">You can add articles to your cart by clicking on the basket icon button on the home page.</p>
      <a href="../index.php" class="btn btn-warning">Go to the home page</a>
    </div>';
} else {
  $carts = $db->get_user_carts($user->user_id);
  $articles = $db->get_user_cart_articles($user->user_id);
  $total = $db->get_total_articles($carts);
  foreach ($articles as $article):
  $quantity = $article["count"];?>
  <div class="card mb-3 mx-5 mt-3 " >
  <div class="d-flex flex-row justify-content-between">
    <div class="d-flex flex-row">
      <img src=<?= $article['picture_link'] ?> class="img-fluid rounded-start" style="height: 100px; width:200px"/>
      <div class="d-flex align-items-center justify-content-center px-5">
        <h5 class="card-title"><?= $article['name'] ?></h5>
      </div>
      <div class="d-flex align-items-center justify-content-center px-5">
        <small class="text-muted">
          <form action="../redirect.php" method="post">
            <input type="hidden" name="article_id" value=<?=$article['article_id'] ?>>
            <input type="hidden" name="user_id" value=<?=$user->user_id?>>
            <button type="submit" name="remove_quantity" style=" border:none ; background:none"><i class="fas fa-minus-circle"></i></button>
            Quantity : <?= $quantity?>      
            <button type="submit" name="add_quantity" style=" border:none ; background:none"><i class="fas fa-plus-circle"></i></button>
          </form>
        </small>
      </div>
      <div class="d-flex align-items-center justify-content-center px-5">
        <h5 class="card-title"><?=$article['price'] * $quantity ?> €</h5>
    </div>
    </div>
    <div class="d-flex flex-row justify-content-evenly">
    <form action="../redirect.php" method="post">
      <div class="d-flex align-items-center justify-content-center px-5">
        <input type="hidden" name="user_id" value=<?=$user->user_id?>>
        <input type="hidden" name="article_id" value=<?=$article['article_id'] ?>>
        <button type="submit" name="remove_item_from_cart"class="btn btn-outline-danger btn-rounded mx-2 mt-4" data-mdb-ripple-color="danger"><i class="fas fa-trash"></i></button>
      </form>
      </div>
    </div>
  </div>  
  </div>
  <?php endforeach;?>
  <div>
    <div class="d-flex flex-row justify-content-end">
      <div class="d-flex flex-row justify-content-end">
        <div class="d-flex align-items-center justify-content-center px-5">
          <h5 class="card-title">Total</h5>
        </div>
        <div class="d-flex align-items-center justify-content-center px-5">
          <h5 class="card-title"><?= $total ?> €</h5>
        </div>
      </div>
    </div>
  </div>
  <div class="d-flex flex-row justify-content-end px-5 py-5">
    <form action="../redirect.php" method="post">
      <button type="submit" name="go_to_validate_card" class="btn btn-outline-warning btn-rounded px-5" data-mdb-ripple-color="warning"><i class="far fa-credit-card text-warning">  Pay </i></button>
    </form>
  </div>
<?php } ?>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.js"></script>
</body>
</html>
