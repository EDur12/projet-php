<html>
<head>
  <meta charset="UTF-8">
  <title>LebonCoinCoin</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet"/>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.css" rel="stylesheet"/>
  <link rel="icon" href="./assets/favicon.ico" type="image/svg+xml">
</head>

<body>

<?php
$isHomePage = false;
$isValidatePage = true;
include '../../header.php';
session_start();
if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
   
}
require_once('../../fonctions/navBar.php');
if (isset($_GET['error'])) {
  echo '<div class="alert alert-danger" role="alert"> You dont have enough money to pay your cart </div>';
}

require_once('../../fonctions/db_manager.php');
$db = new DB_manager("phpmyadmin", "adminphp", "totoduphp", "localhost");
$carts = $db->get_user_carts($user->user_id);
$articles = $db->get_user_cart_articles($user->user_id);
$number_articles = sizeof($articles);
$total = $db->get_total_articles($carts);
?>

<div class="container align-items-center" style="width:50%; margin-left:25%; margin-right:25%">
  <h1 class="text-center m-5" >Validate your order</h1>
  <div class="rounded-pill border border-warning border-2">
      <div class="d-flex justify-content-between m-2">
          <p class="m-2">Number articles: <?=$number_articles?></p>
          <p class="m-2">Total price: <?=$total?>€</p>
      </div>
  </div>
  <form class="row g-3 mt-5" action="../../redirect.php" method="post" style="margin-left:20%; margin-right:20%; border:red;">
      <div class="col-md-6 my-2" style="width:100%">
          <div class="form-outline">
          <input type="text" class="border form-control" required name="facturation_addr"/>
          <label for="validationDefault03" class="form-label">Adress</label>
          </div>
      </div>
      <div class="col-md-6 my-2" style="width:100%">
          <div class="form-outline">
          <input type="text" class="border form-control" required name="facturation_city"/>
          <label for="validationDefault03" class="form-label">City</label>
          </div>
      </div>
      <div class="col-md-6 my-2" style="width:100%">
          <div class="form-outline">
          <input type="text" class="border form-control" required name="postal_code"/>
          <label for="validationDefault05" class="form-label">Zip Code</label>
          </div>
      </div>
      <div class="col-12 my-2">
        <input type=hidden name="user_id" value="<?=$user->user_id?>">
        <input type=hidden name="amount" value="<?=$total?>">
        <input type=hidden name="carts" value="<?=$carts?>">
        <button class="btn btn-warning" type="submit" name="validate_cart" style="width:100%">Pay</button>
      </div>
  </form>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.js"> </script>
</body>
</html>








































