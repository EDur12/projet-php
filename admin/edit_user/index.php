<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>LebonCoinCoin</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet"/>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.css" rel="stylesheet"/>
  <link rel="icon" href="./assets/favicon.ico" type="image/svg+xml">
</head>

<body>
<?php
include '../../header.php';
session_start();
if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
}
$isValidatePage = true;
$isHomePage = false;
require_once('../../fonctions/navBar.php');
require_once('../../fonctions/db_manager.php');
$db = new DB_manager("phpmyadmin", "adminphp", "totoduphp", "localhost");
$user_id=$_GET['user_id'];
$user1 = $db->get_user('user_id', $user_id);
$userPP = base64_encode($user1->profil_pic);
?>
<form action="../../redirect.php" method="post">
    <div class="d-flex align-items-center justify-content-center mt-5">
        <div class="card d-flex flex-row mb-3" style="width: 75%; height: 600px;">
            <div class="card-body" style="margin-left: 25%; margin-right: 25%;">
                <div class="bg-image ripple align-items-center justify-content-center" data-mdb-ripple-color="light">
                    <img src='data:image/jpeg;charset=utf8;base64,<?php echo ($userPP); ?>' class="rounded-circle mb-3" alt="avatar" style="height:150px; width: 150px;"/>
                </div>
                <input type="text" class="form-control my-3" name='name' placeholder="<?=$user1->get_username()?>" style="width:300px"/>
                <input type="text" class="form-control my-3" name='mail' placeholder="<?=$user1->mail?>" style="width:300px"/>
                <input type="text" class="form-control my-3" name='password' placeholder="<?=$user1->password?>" style="width:300px"/>
                <input type="text" class="form-control my-3" name='role' placeholder="<?=$user1->role?>" style="width:300px"/>
                <input type="text" class="form-control my-3" name='pay' placeholder="<?=$user1->pay?>" style="width:300px"/>
                <div class="card-footer text-muted">
                    <div class="d-flex justify-content-between pt-5">
                    <input type="hidden" name="article_id"  value="<?=$user_id?>">
                    <button type="submit" name='cancel_edit_user' class="btn btn-warning">Cancel</button>
                    <button type="submit" name='save_edit_user' class="btn btn-warning">Save changes</button>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.js"></script>
</body>
</html>