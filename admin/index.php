<html>
<head>
  <meta charset="UTF-8">
  <title>LebonCoinCoin</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet"/>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.css" rel="stylesheet"/>
  <link rel="icon" href="./assets/favicon.ico" type="image/svg+xml">
</head>

<body>
<?php
include '../header.php';
session_start();
if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
}
$isValidatePage = false;
$isHomePage = false;
require_once('../fonctions/navBar.php');
require_once('../fonctions/db_manager.php');
$db = new DB_manager("phpmyadmin", "adminphp", "totoduphp", "localhost");
if ($user->role != 'admin') {
    header('Location: /');
}
$articles = $db->get_all_articles();
$users = $db->get_all_users();
?>

<ul class="nav nav-tabs mb-3" id="ex-with-icons" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link active" id="ex-with-icons-tab-1" data-mdb-toggle="tab" href="#ex-with-icons-tabs-1" role="tab"
      aria-controls="ex-with-icons-tabs-1" aria-selected="true"><i class="fas fa-glasses mx-2"></i></i>Articles</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="ex-with-icons-tab-2" data-mdb-toggle="tab" href="#ex-with-icons-tabs-2" role="tab"
      aria-controls="ex-with-icons-tabs-2" aria-selected="false"><i class="fas fa-users mx-2"></i></i>User</a>
  </li>
</ul>

<div class="tab-content" id="ex-with-icons-content">
  <div class="tab-pane fade show active " id="ex-with-icons-tabs-1" role="tabpanel" aria-labelledby="ex-with-icons-tab-1">
  <?php foreach ($articles as $article): 
    $article_picture = base64_encode($article['picture_link']);
    ?>
  <div class="card mb-3 mx-5 mt-3 " >
  <div class="d-flex flex-row justify-content-between">
    <div class="d-flex flex-row">
      <img src='data:image/jpeg;charset=utf8;base64,<?php echo ($article_picture); ?>' class="img-fluid rounded-start" style="height: 100px; width:200px"/>
      <div class="d-flex align-items-center justify-content-center px-5">
        <h5 class="card-title"><?= $article['name'] ?></h5>
      </div>
      <div class="d-flex align-items-center justify-content-center px-5">
        <h5 class="card-title"><?=$article['price']?> €</h5>
      </div>
    </div>
    <div class="d-flex flex-row justify-content-evenly">
    <form action="../redirect.php" method="post">
      <div class="d-flex align-items-center justify-content-center px-5">
        <input type="hidden" name="article_id" value=<?=$article['article_id'] ?>>
        <input type="hidden" name="user_id" value="<?=$user->user_id?>">
        <button type="submit" name="edit_article"class="btn btn-outline-warning btn-rounded mx-2 mt-4" data-mdb-ripple-color="warning"><i class="far fa-edit"></i></i></button>
        <button type="submit" name="delete_article"class="btn btn-outline-danger btn-rounded mx-2 mt-4" data-mdb-ripple-color="danger"><i class="fas fa-trash"></i></button>
      </form>
    </div>
  </div>
  </div>  
  </div>
  <?php endforeach;?>
</div>

<div class="tab-content" id="ex-with-icons-content">
  <div class="tab-pane fade show active " id="ex-with-icons-tabs-2" role="tabpanel" aria-labelledby="ex-with-icons-tab-1">
  <?php foreach ($users as $user1): 
    $user_avatar = base64_encode($user1['profil_pic']);
    ?>

  <div class="card mb-3 mx-5 mt-3 " >
  <div class="d-flex flex-row justify-content-between">
    <div class="d-flex flex-row">
      <img src='data:image/jpeg;charset=utf8;base64,<?php echo ($user_avatar); ?>' class="img-fluid rounded-circle m-3" style="height: 50px; width:50px"/>
      <div class="d-flex align-items-center justify-content-center px-1">
        <h6 class="card-title"><?= $user1['username'] ?></h6>
      </div>
      <div class="d-flex align-items-center justify-content-center px-1">
        <h6 class="card-title"><?=$user1['mail']?></h6>
      </div>
      <div class="d-flex align-items-center justify-content-center px-1">
        <h6 class="card-title"><?=$user1['role']?></h6>
      </div>
    </div>
    <div class="d-flex flex-row justify-content-evenly">
    <form action="../redirect.php" method="post">
      <div class="d-flex align-items-center justify-content-center px-5">
        <input type="hidden" name="user_id" value=<?=$user1['user_id'] ?>>
        <button type="submit" name="edit_user"class="btn btn-outline-warning btn-rounded mx-2 mt-4" data-mdb-ripple-color="warning"><i class="far fa-edit"></i></i></button>
        <button type="submit" name="delete_user"class="btn btn-outline-danger btn-rounded mx-2 mt-4" data-mdb-ripple-color="danger"><i class="fas fa-trash"></i></button>
      </form>
    </div>
  </div>
  </div>  
  </div>
  <?php endforeach;?>
  </div>
</div>





<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.js"></script>
</body>
</html>

