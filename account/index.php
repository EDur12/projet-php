<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>LebonCoinCoin</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.css" rel="stylesheet" />
  <link rel="icon" href="./assets/favicon.ico" type="image/svg+xml">
</head>

<body>
  <?php
  $isValidatePage = false;
  $isHomePage = false;
  include '../header.php';
  session_start();
  if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
    $avatar = base64_encode($user->profil_pic);

  }

  require_once('../fonctions/navBar.php');
  require_once('../fonctions/db_manager.php');
  $db = new DB_manager("phpmyadmin", "adminphp", "totoduphp", "localhost");
  if (isset($_GET['user_id'])) {
    $user_id = $_GET['user_id'];
    $user_page = $db->get_user('user_id', $user_id);
    $user_page_avatar = base64_encode($user_page->profil_pic);
    $user_article = $db->get_user_articles($user_id);
    $count = sizeof($user_article);
    ?>
    <div style="width:70%; margin-left: 15%; margin-right: 15%; margin-top:5%;"
      class="rounded-pill border border-warning border-2">
      <div class="d-flex justify-content-between">
        <div class="d-flex">

          <img src='data:image/jpeg;charset=utf8;base64,<?php echo ($user_page_avatar); ?>' height="30" width="30"
            alt="Profil Picture" />
          <h5 class="m-4">
            <?= $user_page->username ?>
          </h5>
        </div>
        <div>
          <h5 class="m-4">
            <?= $count ?> articles online
          </h5>
        </div>
      </div>
    </div>
    <div class="row row-cols-1 row-cols-md-3 g-4 p-5">
      <?php
      if (empty($user_article)) {
        echo "This user doesn't have any ads yet";
      } else {
        foreach ($user_article as $article): ?>
          <div class="col">
            <div class="card h-100">
              <img style="height:200px;"
                src='data:image/jpeg;charset=utf8;base64,<?php echo (base64_encode($article->picture_link)); ?>'
                class="card-img-top" alt="article_picture" />
              <div class="card-body">
                <h5 class="card-title">
                  <?= $article->name ?>
                </h5>
                <h7 class="card-title">
                  <?= $article->price ?> €
                </h7>
              </div>
              <div class="card-footer">
                <small class="text-muted">
                  <?="Published " . " on " . $article->publication_date ?>
                </small>
                <div class="d-flex justify-content-end">
                  <form action="../redirect.php" method="post">
                    <input type="hidden" id="article_id" name="article_id" value=<?= $article->article_id ?>>
                    <button type="submit" name="go_to_detail" style="border:none; background: none;"><i
                        class="fas fa-plus-circle fa-1x  text-warning"></i></button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach;
      } ?>

    <?php
  } else {
    $invoices = $db->get_user_invoices($user->user_id);
    $articles = $db->get_user_articles($user->user_id);

    ?>

      <!-- Tabs navs -->
      <ul class="nav nav-tabs mb-3" id="ex-with-icons" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" id="ex-with-icons-tab-1" data-mdb-toggle="tab" href="#ex-with-icons-tabs-1"
            role="tab" aria-controls="ex-with-icons-tabs-1" aria-selected="true"><i class="fas fa-newspaper me-2"></i>My
            ads</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" id="ex-with-icons-tab-2" data-mdb-toggle="tab" href="#ex-with-icons-tabs-2" role="tab"
            aria-controls="ex-with-icons-tabs-2" aria-selected="false"><i
              class="fas fa-chart-line fa-fw me-2"></i>Invoices</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" id="ex-with-icons-tab-3" data-mdb-toggle="tab" href="#ex-with-icons-tabs-3" role="tab"
            aria-controls="ex-with-icons-tabs-3" aria-selected="false"><i class="fas fa-cogs fa-fw me-2"></i>Account
            Settings</a>
        </li>
      </ul>
      <!-- Tabs navs -->

      <!-- Tabs content -->
      <div class="tab-content" id="ex-with-icons-content">
        <div class="tab-pane fade show active " id="ex-with-icons-tabs-1" role="tabpanel"
          aria-labelledby="ex-with-icons-tab-1">
          <div class="row row-cols-1 row-cols-md-3 g-4 p-5">
            <?php
            if (empty($articles)) {
              echo "You don't have any ads yet";
            } else {
              foreach ($articles as $article): ?>
                <div class="col">
                  <div class="card text-center">
                    <div class="card-header">ARTICLE</div>
                    <div class="card-body">
                      <h5 class="card-title">
                        <?= $article->name ?>
                      </h5>
                      <p class="card-text"> Price :
                        <?= $article->price ?> €
                      </p>
                      <p class="card-text">
                        <?= $article->description ?>
                      </p>
                    </div>
                    <div class="card-footer text-muted">
                      Publication date :
                      <?= $article->publication_date ?>
                    </div>
                    <form action="../redirect.php" method="post">
                      <input type="hidden" name="article_id" value=<?= $article->article_id ?>>
                      <button type="submit" name="delete_article" style="border:none; background: none;"><i
                          class="fas fa-trash-alt text-danger p-2"></i></button>
                      <button type="submit" name="edit_article" style="border:none; background: none;"><i
                          class="fas fa-edit text-warning p-2"></i></button>
                    </form>
                  </div>
                </div>
              <?php endforeach;
            } ?>
          </div>
        </div>
        <div class="tab-pane fade" id="ex-with-icons-tabs-2" role="tabpanel" aria-labelledby="ex-with-icons-tab-2">
          <div class="row row-cols-1 row-cols-md-3 g-4 p-5">
            <?php
            if (empty($invoices)) {
              echo "You don't have any invoices yet";
            } else {
              foreach ($invoices as $invoice): ?>
                <div class="col">
                  <div class="card text-center">
                    <div class="card-header">INVOICE</div>
                    <div class="card-body">
                      <h5 class="card-title">Ammount :
                        <?= $invoice->amount ?> €
                      </h5>
                      <p class="card-text">
                        <?= $invoice->facturation_addr ?>
                      </p>
                      <p class="card-text">
                        <?= $invoice->postal_code ?>
                      </p>
                      <p class="card-text">
                        <?= $invoice->facturation_city ?>
                      </p>
                    </div>
                    <div class="card-footer text-muted">Transaction date :
                      <?= $invoice->transaction_date ?>
                    </div>
                  </div>
                </div>
              <?php endforeach;
            } ?>
          </div>
        </div>
        <div class="tab-pane fade" id="ex-with-icons-tabs-3" role="tabpanel" aria-labelledby="ex-with-icons-tab-3">
          <div class="d-flex justify-content-center">
            <div>
              <div class="bg-image ripple align-items-center justify-content-center" data-mdb-ripple-color="light">
                <img src="data:image/jpeg;charset=utf8;base64,<?php echo ($avatar); ?>" class="rounded-circle mb-3"
                  alt="avatar" style="height:150px; width: 150px;" />
              </div>
            </div>
          </div>
          <div class="row d-flex justify-content-center">
            <div class="col-md-6">
              <form action="../redirect.php" method="post">
                <div class="input-group mb-3 m-2">
                  <span class="input-group-text" id="basic-addon1">Username</span>
                  <input type="text" class="form-control" name='username' placeholder="<?= $user->username ?>"
                    aria-label="Username" aria-describedby="basic-addon1" />
                </div>
                <div class="input-group mb-3 m-2">
                  <span class="input-group-text" id="basic-addon1"> Mail</span>
                  <input type="text" class="form-control" name='mail' placeholder="<?= $user->mail ?>"
                    aria-label="Username" aria-describedby="basic-addon1" />
                </div>
                <div class="input-group mb-3 m-2">
                  <span class="input-group-text" id="basic-addon1">Password</span>
                  <input type="text" class="form-control" name='password' aria-label="Username"
                    aria-describedby="basic-addon1" />
                </div>
                <div class="input-group mb-3 m-2">
                  <span class="input-group-text" id="basic-addon1">Sold</span>
                  <input type="text" class="form-control" name='sold' placeholder="<?= $user->pay ?> €"
                    aria-label="Username" aria-describedby="basic-addon1" />
                </div>
                <div class="input-group mb-3 m-2">
                  <span class="input-group-text" id="basic-addon1">Profil Picture</span>
                  <input type="file" class="form-control" name='picture' aria-label="Username"
                    aria-describedby="basic-addon1" />
                </div>
                <div class="input-group mb-3 m-2">
                  <input type="hidden" name="user_id" value=<?= $user->user_id ?>>
                  <button type="submit" name="save_change_user" class="btn btn-warning">Save changes</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- Tabs content -->
    <?php } ?>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.js">
    </script>
</body>

</html>