<?php
    require_once('./fonctions/db_manager.php');
    $db = new DB_manager("phpmyadmin", "adminphp", "totoduphp", "localhost");

    if (isset($_POST['go_to_detail'])) {
      $article_id = $_POST['article_id'];
        header("Location: detail/?article_id=$article_id");
    }

    if (isset($_POST['see_profil'])) {
        $user_id = $_POST['user_id'];
        header("Location: account/?user_id=$user_id");
    }

    if (isset($_POST['add_article_to_cart'])) {
        if ($_POST['user_id']==null) {
            header("Location: /register");
        } else {
            $article_id = $_POST['article_id'];
            $user_id = $_POST['user_id'];
            $article = $db->get_article('article_id', $article_id);
            $user = $db->get_user('user_id', $user_id);
            $stock = $db->get_stock('article_id', $article_id);
            if ($stock->quantity > 0) {
                $db->update_stock($stock->stock_id, new Stock($article, $stock->quantity - 1));
                $db->add_cart(new Cart($user, $article));
                header("Location: /cart");
            } else {
                header("Location: detail/?article_id=$article_id&error=1");
            }
        }
    }

    if (isset($_POST['add_quantity'])) {
        $article_id = $_POST['article_id'];
        $user_id = $_POST['user_id'];
        $article = $db->get_article('article_id', $article_id);
        $user = $db->get_user('user_id', $user_id);
        $stock = $db->get_stock('article_id', $article_id);
        if ($stock->quantity > 0) {
            $db->add_cart(new Cart($user, $article));
            $db->update_stock($stock->stock_id, new Stock($article, $stock->quantity - 1));
            header("Location: /cart");
        } else {
            header("Location: cart/?error=1");
        }
    }

    if (isset($_POST['remove_quantity'])) {
        $user_id = $_POST['user_id'];
        $article_id = $_POST['article_id'];
        $article = $db->get_article('article_id', $article_id);
        $cart = $db->get_article_cart($user_id, $article_id);
        $db->remove_cart('cart_id', $cart->cart_id);
        $stock = $db->get_stock('article_id', $article_id);
        $db->update_stock($stock->stock_id, new Stock($article, $stock->quantity + 1));
        header("Location: /cart");
    }

    
    if (isset($_POST['remove_item_from_cart'])) {
        $article_id = $_POST['article_id'];
        $db->remove_cart('article_id', $article_id);
        $stock = $db->get_stock('article_id', $article_id);
        $stock->set_quantity($stock->quantity + 1);
        header("Location: /cart");
    }

    if (isset($_POST['save_change_user'])) {
        $user_id = $_POST['user_id'];
        $user_username = $_POST['username'];
        $user_mail = $_POST['mail'];
        $user_password = $_POST['password'];
        $user_sold = $_POST['sold'];
        $user_picture = $_POST['picture'];
        $user = $db->get_user('user_id', $user_id);
        if ($user_username == null) {
          $user_username = $user->username;
        }
        if ($user_mail == null) {
          $user_mail = $user->mail;
        }
        if ($user_password == null) {
          $user_password = $user->password;
        }
        if ($user_sold == null) {
          $user_sold = $user->pay;
        }
        if ($user_picture == null) {
          $user_picture = $user->profil_pic;
        }
        $new_user = new User($user_username, $user_password, $user_mail,$user->role,$user_picture, $user_sold);
        $db->update_user($user,$new_user);
        header("Location: account/");
    }

    if (isset($_POST['delete_article'])) {
        $article_id = $_POST['article_id'];
        $db->remove_article('article_id', $article_id);
        $db->remove_cart('article_id', $article_id);
        $db->remove_stock('article_id', $article_id);
        if (isset($_POST['user_id'])) {
            header("Location: admin/");
        } else {
            header("Location: account/");
        }
        
    }

    if (isset($_POST['delete_user'])) {
        $user_id = $_POST['user_id'];
        $articles = $db->get_user_articles($user_id);
        foreach ($carts as $cart) {
            $stock = $db->get_stock('article_id', $cart->article_id);
            $article = $db->get_article('article_id', $cart->article_id);
            $db->update_stock($stock->stock_id, new Stock($article, $stock->quantity + 1));
            $db->remove_cart('cart_id', $cart->cart_id);
        }
        foreach ($articles as $article) {
            $db->remove_article('article_id', $article->article_id);
            $db->remove_cart('article_id', $article->article_id);
            $db->remove_stock('article_id', $article->article_id);
        }
        $carts = $db->get_user_carts($user_id);
        
        $db->remove_user('user_id', $user_id);
        header("Location: admin/");
        
        
    }

    if (isset($_POST['edit_article'])) {
        $article_id = $_POST['article_id'];
        header("Location: edit/?article_id=$article_id");
    }

    if (isset($_POST['save_edit_article'])) {
        $article_id = $_POST['article_id'];
        $name = $_POST['name'];
        $price = $_POST['price'];
        $description = $_POST['description'];
        $picture_link = $_POST['picture_link'];
        $article = $db->get_article('article_id', $article_id);
        if ($name == null) {
          $name = $article->name;
        }
        if ($price == null) {
          $price = $article->price;
        }
        if ($description == null) {
          $description = $article->description;
        }
        if ($picture_link == null) {
          $picture_link = $article->picture_link;
        }
        $new_article = new Article($name, $description,$price, $article->publication_date, $article->author, $article->picture_link);
        $db->update_article($article->article_id,$new_article);
        $user = $db->get_user('user_id', $_POST['user_id']);
        if ($user->role == 'admin'){
            header("Location: admin/");
        } else {
            header("Location: account/");
        }
    }

    if (isset($_POST['cancel_edit_article'])) {
        $user = $db->get_user('user_id', $_POST['user_id']);
        if ($user->role == 'admin'){
            header("Location: admin/");
        } else {
            header("Location: account/");
        }
    }

    if (isset($_POST['sell_article'])) {
        $user = $db->get_user('user_id', $_POST['user_id']);
        $article_name = $_POST['article_name'];
        $article_price = $_POST['article_price'];
        $article_description = $_POST['article_description'];
        $quantity = $_POST['quantity'];
        $picture_link = $_POST['picture_link'];
        $article = new Article($article_name, $article_description,$article_price, date("Y-m-d"), $user, $picture_link);
        $db->add_article($article);
        $article = $db->get_last_article(); 
        if ($quantity == null) {
          $quantity = 1;
        }
        $stock = new Stock($article, $quantity);    
        $db->add_stock($stock);
        header("Location: /");
    }

    if (isset($_POST['go_to_validate_card'])) {
        $user_id = $_POST['user_id'];
        header("Location: /cart/validate");
    }

    if (isset($_POST['validate_cart'])) {
        $user_id = $_POST['user_id'];
        echo $user_id;
        $user = $db->get_user('user_id', $user_id);
        $amount = $_POST['amount'];
        $carts = $_POST['carts'];
        $facturation_addr = $_POST['facturation_addr'];
        $facturation_city = $_POST['facturation_city'];
        $postal_code = $_POST['postal_code'];
        if ($user->pay >= $amount) {
            $invoice = new Invoice($user, date("Y-m-d"), $amount, $facturation_addr, $facturation_city, $postal_code);
            $db->add_invoice($invoice);
            $db->remove_cart('user_id', $user_id);
            $user->set_pay($user->pay - $amount);
            header("Location: /account");
        } else {
            header("Location:/cart/validate?error=1");
        }
    }

    if (isset($_POST['search_article'])) {
        $search = $_POST['search'];
        header("Location: /?search=$search");
    }

    if (isset($_POST['delete_user'])) {
        $user_id = $_POST['user_id'];
        $db->remove_user('user_id', $user_id);
        header("Location: /admin");
    }

    if (isset($_POST['edit_user'])) {
        header("Location: /admin/edit_user/?user_id=$_POST[user_id]");
    }

    if (isset($_POST['save_edit_user'])) {
        $user_id = $_POST['user_id'];
        $user = $db->get_user('user_id', $user_id);
        $new_user = new User($user->username, $user->password, $user->mail, $user->pay, $user->role);
        if ($_POST['username'] != null) {
            $new_user->set_username($_POST['username']);
        }
        if ($_POST['password'] != null) {
            $new_user->set_password($_POST['password']);
        }
        if ($_POST['email'] != null) {
            $new_user->set_email($_POST['email']);
        }
        if ($_POST['pay'] != null) {
            $new_user->set_pay($_POST['pay']);
        }
        if ($_POST['role'] != null) {
            $new_user->set_role($_POST['role']);
        }
        $db->update_user($user,$new_user);
        header("Location: /admin");
    }

    if (isset($_POST['cancel_edit_user'])) {
        header("Location: /admin");
    }
?>