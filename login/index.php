<html>

<head>
  <meta charset="UTF-8">
  <title>LebonCoinCoin</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet"/>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.css" rel="stylesheet"/>
  <link rel="icon" href="./assets/favicon.ico" type="image/svg+xml">
</head>

<body>
<?php
$isValidatePage = false;
$isHomePage = false;
require_once('../fonctions/navBar.php');
if (isset($_GET['error'])) {
  echo '<div class="alert alert-danger" role="alert"> Wrong password or username </div>';
}
?>

<div class="d-flex justify-content-center mt-5" >
  <img src="../assets/logo.png" height="100" alt="Logo" loading="lazy"/>
</div>
<form class="justify-content-center mt-5" style="width:50%; margin-left: 25%; margin-right:25%" action="../header.php" method="post">
  <div class="input-group mb-3">
    <span class="input-group-text" id="basic-addon1"><i class="far fa-user"></i></span>
    <input type="text" class="form-control" placeholder="Username" name ="username" aria-label="Username" aria-describedby="basic-addon1"/>
  </div>
  <div class="input-group mb-3">
    <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
    <input type="password" class="form-control" name="password" placeholder="Password" aria-label="Username" aria-describedby="basic-addon1"/>
  </div>
  <button type="submit" name="login" class="btn btn-warning" style="width:100%">Login</button>
  <div class="small">
    Don't have an account ?  
    <a href="/register">Register</a>
  </div>
</form>
</body>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.js">
  </script>
</body>
</html>



