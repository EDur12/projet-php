<?php
if (isset($_SESSION['user'])) {
  $user = $_SESSION['user'];
  $profil_pic = base64_encode($user->profil_pic);
  $isConnected = true;
} else {
  $isConnected = false;
}
if ($isConnected == true && $isHomePage == true) {
  echo '<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <!-- Container wrapper -->
  <div class="container-fluid">
    <!-- Collapsible wrapper -->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <!-- Navbar brand -->
      <a class="navbar-brand mt-2 mt-lg-0" href="/"> <img src="../assets/logo.png" height="30" alt="Logo" loading="lazy"/></a>
      <!-- Left links -->
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item"><a class="nav-link" href="/">Home</a></li>
        <li class="nav-item">
          <a type="button" class="btn btn-outline-warning btn-rounded mx-4" data-mdb-ripple-color="dark" href="/sell">
          <i class="fas fa-plus"></i>
          Add an article
          </a>
        </li>
      </ul>
      <!-- Left links -->
    </div>
    <!-- Collapsible wrapper -->

    <!-- Right elements -->
    <div class="d-flex align-items-center">
    <!-- Search Bar -->
    <form class="d-flex input-group w-auto pt-2" action="./redirect.php" method="post">
        <input type="search" name="search" class="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon"/>
        <button type="submit" name="search_article" class="btn  shadow-0"><i class="fas fa-search"></i></button>
    </form>
      <!-- Icon -->
      <a class="text-reset me-3 ms-3" href="/cart">
        <i class="fas fa-shopping-cart"></i>
      </a>
      <!-- Avatar -->
      <div class="dropdown">
        <a class="dropdown-toggle d-flex align-items-center hidden-arrow" href="#" id="navbarDropdownMenuAvatar" role="button" data-mdb-toggle="dropdown" aria-expanded="false">
        <img src="data:image/jpeg;charset=utf8;base64,' . ($profil_pic). '" class="rounded-circle" height="25" width="25" alt="Profil Picture"  style="margin-left: 10px;"/>
        </a>
        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdownMenuAvatar">
          <li><a class="dropdown-item" href="/account">My profile</a></li>
          <li><form action="../header.php" method="post">
            <button type="submit" name="logout" class="dropdown-item">Logout</button></form>
          </li>
        </ul>
      </div>
    </div>
    <!-- Right elements -->
  </div>
  <!-- Container wrapper -->
</nav>';
} else if ($isConnected == true && $isValidatePage == true) {
  echo '<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <!-- Container wrapper -->
    <div class="container-fluid">
      <!-- Collapsible wrapper -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Navbar brand -->
        <a class="navbar-brand mt-2 mt-lg-0" href="/"> <img src="../../assets/logo.png" height="30" alt="Logo" loading="lazy"/></a>
        <!-- Left links -->
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item"><a class="nav-link" href="/">Home</a></li>
          <li class="nav-item">
            <a type="button" class="btn btn-outline-warning btn-rounded mx-4" data-mdb-ripple-color="dark" href="/sell">
            <i class="fas fa-plus"></i>
            Add an article
            </a>
          </li>
        </ul>
        <!-- Left links -->
      </div>
      <!-- Collapsible wrapper -->
      <!-- Right elements -->
      <div class="d-flex align-items-center">
      <!-- Search Bar -->
        <!-- Icon -->
        <a class="text-reset me-3 ms-3" href="/cart">
          <i class="fas fa-shopping-cart"></i>
        </a>
        <!-- Avatar -->
        <div class="dropdown">
          <a class="dropdown-toggle d-flex align-items-center hidden-arrow" href="#" id="navbarDropdownMenuAvatar" role="button" data-mdb-toggle="dropdown" aria-expanded="false">
          <img src="data:image/jpeg;charset=utf8;base64,' . ($profil_pic). '" class="rounded-circle" height="25" alt="Profil Picture" loading="lazy" style="margin-left: 10px;"/>
          </a>
          <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdownMenuAvatar">
            <li><a class="dropdown-item" href="/account">My profile</a></li>
            <li><a class="dropdown-item" href="#">Logout</a></li>
          </ul>
        </div>
      </div>
      <!-- Right elements -->
    </div>
    <!-- Container wrapper -->
  </nav>';
} else if ($isConnected == false && $isHomePage == false) {
  echo '<!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <!-- Container wrapper -->
    <div class="container-fluid">
      <!-- Toggle button -->
      <button
        class="navbar-toggler"
        type="button"
        data-mdb-toggle="collapse"
        data-mdb-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <i class="fas fa-bars"></i>
      </button>
  
      <!-- Collapsible wrapper -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Navbar brand -->
        <a class="navbar-brand mt-2 mt-lg-0" href="/">
          <img
            src="../assets/logo.png"
            height="30"
            alt="Logo"
            loading="lazy"
          />
        </a>
        <!-- Left links -->
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="/">Home</a>
          </li>
        </ul>
        <!-- Left links -->
      </div>
      <!-- Collapsible wrapper -->
      <!-- Right elements -->
      <div class="d-flex align-items-center">
        <!-- Avatar -->
        <div class="dropdown">
          <a href="/login"><i class="far fa-user-circle fa-1x text-muted"></i></a>
        </div>
      </div>
      <!-- Right elements -->
    </div>
    <!-- Container wrapper -->
  </nav>
  <!-- Navbar -->';
} else if ($isConnected == true && $isHomePage == false) {
  echo "<nav class='navbar navbar-expand-lg navbar-light bg-light'>
    <!-- Container wrapper -->
    <div class='container-fluid'>
      <!-- Collapsible wrapper -->
      <div class='collapse navbar-collapse' id='navbarSupportedContent'>
        <!-- Navbar brand -->
        <a class='navbar-brand mt-2 mt-lg-0' href='/'> <img src='../assets/logo.png' height='30' alt='Logo' loading='lazy'/></a>
        <!-- Left links -->
        <ul class='navbar-nav me-auto mb-2 mb-lg-0'>
          <li class='nav-item'><a class='nav-link' href='/'>Home</a></li>
          <li class='nav-item'>
            <a type='button' class='btn btn-outline-warning btn-rounded mx-4' data-mdb-ripple-color='dark' href='/sell'>
            <i class='fas fa-plus'></i>
            Add an article
            </a>
          </li>
        </ul>
        <!-- Left links -->
      </div>
      <!-- Collapsible wrapper -->
      <!-- Right elements -->
      <div class='d-flex align-items-center'>
      <!-- Search Bar -->
        <!-- Icon -->
        <a class='text-reset me-3 ms-3' href='/cart'>
          <i class='fas fa-shopping-cart'></i>
        </a>
        <!-- Avatar -->
        <div class='dropdown'>
          <a class='dropdown-toggle d-flex align-items-center hidden-arrow' href='#' id='navbarDropdownMenuAvatar' role='button' data-mdb-toggle='dropdown' aria-expanded='false'>
          <img src='data:image/jpeg;charset=utf8;base64," . ($profil_pic). "' class='rounded-circle' height='25' alt='Profil Picture' loading='lazy' style='margin-left: 10px;'/>
          </a>
          <ul class='dropdown-menu dropdown-menu-end' aria-labelledby='navbarDropdownMenuAvatar'>
            <li><a class='dropdown-item' href='/account'>My profile</a></li>
            <li><form action='../header.php' method='post'>
            <button type='submit' name='logout' class='dropdown-item'>Logout</button></form>
            </li>
          </ul>
        </div>
      </div>
      <!-- Right elements -->
    </div>
    <!-- Container wrapper -->
  </nav>";
} else if ($isConnected == false && $isHomePage == true) {
  echo '<!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <!-- Container wrapper -->
      <div class="container-fluid">
        <!-- Toggle button -->
        <button
          class="navbar-toggler"
          type="button"
          data-mdb-toggle="collapse"
          data-mdb-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <i class="fas fa-bars"></i>
        </button>
    
        <!-- Collapsible wrapper -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Navbar brand -->
          <a class="navbar-brand mt-2 mt-lg-0" href="/">
            <img
              src="../assets/logo.png"
              height="30"
              alt="Logo"
              loading="lazy"
            />
          </a>
          <!-- Left links -->
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="/">Home</a>
            </li>
          </ul>
          <!-- Left links -->
        </div>
        <!-- Collapsible wrapper -->
        <!-- Right elements -->
        <div class="d-flex align-items-center">
        <!-- Search Bar -->
        <form class="d-flex input-group w-auto pt-2" action="./redirect.php" method="post">
            <input type="search" name="search" class="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon"/>
            <button type="submit" name="search_article" class="btn  shadow-0"><i class="fas fa-search"></i></button>
        </form>
          <!-- Avatar -->
          <div class="dropdown">
            <a href="/login"><i class="far fa-user-circle fa-1x text-muted"></i></a>
          </div>
        </div>
        <!-- Right elements -->
      </div>
      <!-- Container wrapper -->
    </nav>
    <!-- Navbar -->';
}
?>