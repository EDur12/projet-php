<?php
// path : data_structure.php

// user class
//------------------------------------------------------------------
class User
{
    public $user_id;
    public $username;
    public $password;
    public $mail;
    public $pay;
    public $profil_pic;
    public $role;

    function set_auto_pic($tag)
    {
        $curl = curl_init();
        $api_key = "c9ZaQ09f7RgebTnygNQDk9CBpJkiQvEf";
        curl_setopt($curl, CURLOPT_URL, "https://api.giphy.com/v1/gifs/random?api_key=$api_key&tag=$tag");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($result, true);
        $link = $result["data"]["images"]["original"]["url"];
        $image = file_get_contents($link);
        $this->profil_pic = $image;
    }

    function __construct($username, $password, $mail, $role, $profil_pic, $pay = 0)
    {
        $this->username = $username;
        $this->password = $password;
        $this->mail = $mail;
        $this->pay = $pay;
        $this->role = $role;
        $this->profil_pic = $profil_pic;
    }

    // Methods
    function get_user_id()
    {
        return $this->user_id;
    }
    function set_user_id($user_id)
    {
        $this->user_id = $user_id;
    }
    function set_username($username)
    {
        $this->username = $username;
    }
    function get_username()
    {
        return $this->username;
    }
    function set_password($password)
    {
        $this->password = $password;
    }
    function get_password()
    {
        return $this->password;
    }
    function set_email($mail)
    {
        $this->mail = $mail;
    }
    function get_email()
    {
        return $this->mail;
    }
    function set_pay($pay)
    {
        $this->pay = $pay;
    }
    function get_pay()
    {
        return $this->pay;
    }
    function set_profil_pic($profil_pic)
    {
        $this->profil_pic = $profil_pic;
    }

    function get_profil_pic()
    {
        return $this->profil_pic;
    }
    function set_role($role)
    {
        $this->role = $role;
    }
    function get_role()
    {
        return $this->role;
    }
}

// article class
//------------------------------------------------------------------
class Article
{
    public $article_id;
    public $name;
    public $description;
    public $price;
    public $publication_date;
    public $picture_link;
    public $author;

    function set_auto_pic($tag = "cat")
    {
        $curl = curl_init();
        $api_key = "c9ZaQ09f7RgebTnygNQDk9CBpJkiQvEf";
        curl_setopt($curl, CURLOPT_URL, "https://api.giphy.com/v1/gifs/random?api_key=$api_key&tag=$tag");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($result, true);
        $this->picture_link = $result["data"]["images"]["original"]["url"];
    }
    function __construct($name, $description, $price, $publication_date, $user, $picture_link)
    {
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
        $this->publication_date = $publication_date;
        $this->author = $user;
        $this->picture_link = $picture_link;
    }
    // Methods
    function get_article_id()
    {
        return $this->article_id;
    }
    function set_article_id($article_id)
    {
        $this->article_id = $article_id;
    }
    function set_name($name)
    {
        $this->name = $name;
    }
    function get_name()
    {
        return $this->name;
    }
    function set_description($description)
    {
        $this->description = $description;
    }
    function get_description()
    {
        return $this->description;
    }
    function set_price($price)
    {
        $this->price = $price;
    }
    function get_price()
    {
        return $this->price;
    }
    function set_publication_date($publication_date)
    {
        $this->publication_date = $publication_date;
    }
    function get_publication_date()
    {
        return $this->publication_date;
    }
    function set_author($author)
    {
        $this->author = $author;
    }
    function get_author()
    {
        return $this->author;
    }
    function set_picture_link($picture_link)
    {
        $this->picture_link = $picture_link;
    }
    function get_picture_link()
    {
        return $this->picture_link;
    }
}
//------------------------------------------------------------------
// cart class
class Cart
{
    public $cart_id;
    public $user;
    public $article;
   

    // Methods
    function __construct($user, $article)
    {
        $this->article = $article;
        $this->user = $user;
    }
    function set_cart_id($cart_id)
    {
        $this->cart_id = $cart_id;
    }
    function get_cart_id()
    {
        return $this->cart_id;
    }
    function set_article($article)
    {
        $this->article = $article;
    }
    function get_article()
    {
        return $this->article;
    }
    function set_user($user)
    {
        $this->user = $user;
    }
    function get_user()
    {
        return $this->user;
    }
}

class Invoice
{
    public $invoice_id;
    public $user;
    public $transaction_date;
    public $amount;
    public $facturation_addr;
    public $facturation_city;
    public $postal_code;

    // Methods
    function __construct($user, $transaction_date, $amount, $facturation_addr, $facturation_city, $postal_code)
    {
        $this->user = $user;
        $this->transaction_date = $transaction_date;
        $this->amount = $amount;
        $this->facturation_addr = $facturation_addr;
        $this->facturation_city = $facturation_city;
        $this->postal_code = $postal_code;
    }
    function get_invoice_id()
    {
        return $this->invoice_id;
    }
    function set_invoice_id($invoice_id)
    {
        $this->invoice_id = $invoice_id;
    }
    function set_user($user)
    {
        $this->user = $user;
    }
    function get_user()
    {
        return $this->user;
    }
    function set_transaction_date($transaction_date)
    {
        $this->transaction_date = $transaction_date;
    }
    function get_transaction_date()
    {
        return $this->transaction_date;
    }
    function set_amount($amount)
    {
        $this->amount = $amount;
    }
    function get_amount()
    {
        return $this->amount;
    }
    function set_facturation_addr($facturation_addr)
    {
        $this->facturation_addr = $facturation_addr;
    }
    function get_facturation_addr()
    {
        return $this->facturation_addr;
    }
    function set_facturation_city($facturation_city)
    {
        $this->facturation_city = $facturation_city;
    }
    function get_facturation_city()
    {
        return $this->facturation_city;
    }
    function set_postal_code($postal_code)
    {
        $this->postal_code = $postal_code;
    }
    function get_postal_code()
    {
        return $this->postal_code;
    }
}
class Stock
{
    public $stock_id;
    public $article;
    public $quantity;

    // Methods
    function __construct($article, $quantity)
    {
        $this->article = $article;
        $this->quantity = $quantity;
    }
    function get_stock_id()
    {
        return $this->stock_id;
    }
    function set_stock_id($stock_id)
    {
        $this->stock_id = $stock_id;
    }
    function set_article($article)
    {
        $this->article = $article;
    }
    function get_article()
    {
        return $this->article;
    }
    function set_quantity($quantity)
    {
        $this->quantity = $quantity;
    }
    function get_quantity()
    {
        return $this->quantity;
    }
}
?>