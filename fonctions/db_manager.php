<?php
// include data_structure.php
include_once "data_structure.php";
// database manager class:
class DB_manager
{
    public $name;
    public $user;
    public $password;
    public $host;
    public $mysqli;

    // constructor
    function __construct($name, $user, $password, $host)
    {
        $this->name = $name;
        $this->user = $user;
        $this->password = $password;
        $this->host = $host;
        $this->connect();
    }
    function get_mysqli()
    {
        return $this->mysqli;
    }
    // this function is used to connect to the database
    function connect()
    {
        $this->mysqli = new mysqli($this->host, $this->user, $this->password, $this->name, 3306);
        if ($this->mysqli->connect_errno) {
            echo "Failed to connect to MySQL: " . $this->mysqli->connect_error;
        } else {
            //echo "Connection to MySQL established!\n";
        }
    }

    function disconnect()
    {
        $this->mysqli->close();
        echo "Connection to MySQL closed!\n";
    }

    // ----------------- USER -----------------
    // this function add a new user to the database
    function add_user($user)
    {
        // check if user already exists
        $isUnique = true;
        $query = "SELECT * FROM user WHERE username = '$user->username'";
        $result = $this->mysqli->query($query);
        if ($result->num_rows > 0) {
            $isUnique = false;
            #echo "User ($user->username) already exists!\n";
        }
        $query = "SELECT * FROM user WHERE mail = '$user->mail'";
        $result = $this->mysqli->query($query);
        if ($result->num_rows > 0) {
            $isUnique = false;
            #echo "User ($user->mail) already exists!\n";
        }
        if ($isUnique) {
            $query = "INSERT INTO user (username, password, mail, role, profil_pic, pay) VALUES ('$user->username', '$user->password', '$user->mail', '$user->role','$user->profil_pic',0)";
            $this->mysqli->query($query);
            // error handling
            // if ($this->mysqli->errno) {
            //     echo "Error: " . $this->mysqli->error;
            // } else {
            //     echo "User ($user->username) added to database!\n";
            // }
        }
    }
    // select user from database by username and return a User object
    function get_user($where, $equals)
    {
        $query = "SELECT * FROM user WHERE $where = '$equals'";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $row = $result->fetch_assoc();
            $user = new User($row["username"], $row["password"], $row["mail"], $row["role"], $row["profil_pic"], $row["pay"]);
            $user->set_user_id($row["user_id"]);
            $user->set_profil_pic($row["profil_pic"]);
            return $user;
        }
    }
    function get_user_passwd($where, $equals)
    {
        $query = "SELECT password FROM user WHERE $where = '$equals'";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $row = $result->fetch_assoc();
            return $row["password"];
        }
    }
    function get_last_user()
    {
        $query = "SELECT * FROM user ORDER BY user_id DESC LIMIT 1";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $row = $result->fetch_assoc();
            $user = new User($row["username"], $row["password"], $row["mail"], $row["role"], $row["profil_pic"], $row["pay"]);
            $user->set_user_id($row["user_id"]);
            $user->set_profil_pic($row["profil_pic"]);
            return $user;
        }
    }
    // this function remove a user from the database
    function remove_user($where, $equals)
    {
        $query = "DELETE FROM user WHERE $where = '$equals'";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
        } else {
            echo "User ($where = $equals) removed!\n";
        }
    }
    function removeAllusers()
    {
        $query = "DELETE FROM user";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
        } else {
            echo "All users removed!\n";
        }
    }
    function update_user($base_user, $new_user)
    {
        $query = "UPDATE user SET username = '$new_user->username', password = '$new_user->password', mail = '$new_user->mail', role = '$new_user->role', profil_pic = '$new_user->profil_pic', pay = '$new_user->pay' WHERE user_id = '$base_user->user_id'";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
        } else {
            echo "User ($base_user->username) updated!\n";
        }
    }

    function get_all_users()
    {
        $query = "SELECT * FROM user ORDER BY user.user_id DESC LIMIT 20";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $users = array();
            while ($row = $result->fetch_assoc()) {
                $user = array();
                $user["username"] = $row["username"];
                $user["role"] = $row["role"];
                $user["pay"] = $row["pay"];
                $user["password"] = $row["password"];
                $user["mail"] = $row["mail"];
                $user["profil_pic"] = $row["profil_pic"];
                $user["user_id"] = $row["user_id"];
                array_push($users, $user);
            }
            return $users;
        }
    }

    // -------------------------------------------
    // ----------------- ARTICLE -----------------
    // this function add a new article to the database
    function add_article($article)
    {
        $user_id = $article->author->user_id;
        $query = "INSERT INTO article (article_id,name,description,price,publication_date,user_id,picture_link) VALUES (NULL,'$article->name','$article->description','$article->price','$article->publication_date','$user_id','$article->picture_link')";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error . "\n";
        } else {
            echo "Article ($article->name) added!\n";
        }
    }
    function get_article($where, $equals)
    {
        $query = "SELECT * FROM article WHERE $where = '$equals' ORDER BY publication_date DESC";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $row = $result->fetch_assoc();
            $article = new Article($row["name"], $row["description"], $row["price"], $row["publication_date"], $row["user_id"], $row["picture_link"]);
            $article->set_article_id($row["article_id"]);
            $article->set_author($this->get_user("user_id", $row["user_id"]));
            return $article;
        }
    }
    function get_user_articles($user_id)
    {
        $query = "SELECT * FROM article WHERE user_id = '$user_id' ORDER BY publication_date DESC";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $articles = array();
            while ($row = $result->fetch_assoc()) {
                $article = new Article($row["name"], $row["description"], $row["price"], $row["publication_date"], $row["user_id"], $row["picture_link"]);
                $article->set_article_id($row["article_id"]);
                $article->set_author($this->get_user("user_id", $row["user_id"]));
                array_push($articles, $article);
            }
            return $articles;
        }
    }
    function get_last_article()
    {
        $query = "SELECT * FROM article ORDER BY article_id DESC LIMIT 1";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $row = $result->fetch_assoc();
            $article = new Article($row["name"], $row["description"], $row["price"], $row["publication_date"], $row["user_id"], $row["picture_link"]);
            $article->set_article_id($row["article_id"]);
            $article->set_author($this->get_user("user_id", $row["user_id"]));
            return $article;
        }
    }
    function update_article($base_article_id, $newArticle)
    {
        $query = "UPDATE article SET name = '$newArticle->name', description = '$newArticle->description', price = '$newArticle->price', publication_date = '$newArticle->publication_date', picture_link = '$newArticle->picture_link' WHERE article_id = '$base_article_id'";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
        } else {
            echo "Article ($base_article_id) updated!\n";
        }
    }
    // remove article from database
    function remove_article($where, $equals)
    {
        $query = "DELETE FROM article WHERE $where = '$equals'";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
        } else {
            echo "Article ($where = $equals) removed!\n";
        }
    }
    // get home article list in order of publication date
    function get_home_articles()
    {
        $query = "SELECT article.article_id, article.name, article.description, article.price, article.publication_date, article.user_id, article.picture_link, user.username FROM article INNER JOIN user ON article.user_id = user.user_id INNER JOIN stock ON stock.article_id = article.article_id AND stock.quantity >0 ORDER BY article.publication_date DESC LIMIT 20";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $articles = array();
            while ($row = $result->fetch_assoc()) {
                $article = array();
                $article["name"] = $row["name"];
                $article["description"] = $row["description"];
                $article["price"] = $row["price"];
                $article["publication_date"] = $row["publication_date"];
                $article["picture_link"] = $row["picture_link"];
                $article["username"] = $row["username"];
                $article["article_id"] = $row["article_id"];
                array_push($articles, $article);
            }
            return $articles;
        }
    }

    function get_all_articles()
    {
        $query = "SELECT * FROM article ORDER BY article.publication_date DESC LIMIT 20";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $articles = array();
            while ($row = $result->fetch_assoc()) {
                $article = array();
                $article["name"] = $row["name"];
                $article["description"] = $row["description"];
                $article["price"] = $row["price"];
                $article["publication_date"] = $row["publication_date"];
                $article["picture_link"] = $row["picture_link"];
                $article["article_id"] = $row["article_id"];
                array_push($articles, $article);
            }
            return $articles;
        }
    }
    // -------------------------------------------
    // ------------------ CART -------------------
    // add cart
    function add_cart($cart)
    {
        $article_id = $cart->article->article_id;
        $user_id = $cart->user->user_id;
        $query = "INSERT INTO cart (cart_id,article_id,user_id) VALUES (NULL,'$article_id','$user_id')";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error . "\n";
        } else {
            $name = $cart->article->name;
            echo "Cart ($name) added!\n";
        }
    }
    // get one cart
    function get_cart($where, $equals)
    {
        $query = "SELECT * FROM cart WHERE $where = '$equals'";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $row = $result->fetch_assoc();
            $cart = new Cart($this->get_article("article_id", $row["article_id"]), $this->get_user("user_id", $row["user_id"]));
            $cart->set_cart_id($row["cart_id"]);
            return $cart;
        }
    }

    function get_user_carts($user_id)
    {
        $query = "SELECT * FROM cart WHERE user_id = '$user_id'";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $carts = array();
            while ($row = $result->fetch_assoc()) {
                $cart = new Cart($row["user_id"], $row["article_id"]);
                $cart->set_cart_id($row["cart_id"]);
                array_push($carts, $cart);
            }
            return $carts;
        }
    }

    function get_article_cart($user_id, $article_id)
    {
        $query = "SELECT * FROM cart WHERE user_id = '$user_id' AND article_id = '$article_id' ORDER BY cart_id DESC LIMIT 1";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $row = $result->fetch_assoc();
            $cart = new Cart($this->get_article("article_id", $row["article_id"]), $this->get_user("user_id", $row["user_id"]));
            $cart->set_cart_id($row["cart_id"]);
            return $cart;
        }
    }
    // get last cart (test)
    function get_last_cart()
    {
        $query = "SELECT * FROM cart ORDER BY cart_id DESC LIMIT 1";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $row = $result->fetch_assoc();
            $cart = new Cart($this->get_article("article_id", $row["article_id"]), $this->get_user("user_id", $row["user_id"]));
            $cart->set_cart_id($row["cart_id"]);
            return $cart;
        }
    }
    // get user cart list
    function get_user_cart_articles($user_id)
    {
        $query = "SELECT DISTINCT article.article_id, article.name, article.description, article.price, article.publication_date, 
        article.user_id, article.picture_link, user.username,COUNT(cart.article_id) AS count
        FROM article
        INNER JOIN user ON article.user_id = user.user_id
        INNER JOIN cart ON article.article_id = cart.article_id
        WHERE cart.user_id = $user_id
        GROUP BY article.article_id
        ORDER BY article.publication_date DESC";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $articles = array();
            while ($row = $result->fetch_assoc()) {
                $article = array();
                $article["name"] = $row["name"];
                $article["description"] = $row["description"];
                $article["price"] = $row["price"];
                $article["publication_date"] = $row["publication_date"];
                $article["picture_link"] = $row["picture_link"];
                $article["username"] = $row["username"];
                $article["article_id"] = $row["article_id"];
                $article["count"] = $row["count"];
                array_push($articles, $article);
            }
            return $articles;
        }
    }

    function get_total_articles($carts)
    {
        $total = 0;
        foreach ($carts as $cart) {
            $article = $this->get_article('article_id', $cart->article);
            $total += $article->price;
        }
        return $total;
    }

    // remove cart
    function remove_cart($where, $equals)
    {
        $query = "DELETE FROM cart WHERE $where = '$equals'";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
        } else {
            echo "Cart ($where = $equals) removed!\n";
        }
    }
    // -------------------------------------------
    // ----------------- INVOICE -----------------
    // add one invoice
    function add_invoice($invoice)
    {
        $user_id = $invoice->user->user_id;
        $query = "INSERT INTO invoice (invoice_id,user_id,transaction_date,amount,facturation_addr,facturation_city,postal_code) VALUES (NULL,'$user_id','$invoice->transaction_date','$invoice->amount','$invoice->facturation_addr','$invoice->facturation_city','$invoice->postal_code')";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error . "\n";
        } else {
            echo "Invoice ($invoice->invoice_id) added!\n";
        }
    }
    // get one invoice
    function get_invoice($where, $equals)
    {
        $query = "SELECT * FROM invoice WHERE $where = '$equals'";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $row = $result->fetch_assoc();
            $invoice = new Invoice($this->get_user("user_id", $row["user_id"]), $row["transaction_date"], $row["amount"], $row["facturation_addr"], $row["facturation_city"], $row["postal_code"]);
            $invoice->set_invoice_id($row["invoice_id"]);
            return $invoice;
        }
    }
    // get all invoices of a user
    function get_user_invoices($user_id)
    {
        $query = "SELECT * FROM invoice WHERE user_id = '$user_id' ORDER BY transaction_date DESC";
        $result = $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $invoices = array();
            while ($row = $result->fetch_assoc()) {
                $invoice = new Invoice($this->get_user("user_id", $row["user_id"]), $row["transaction_date"], $row["amount"], $row["facturation_addr"], $row["facturation_city"], $row["postal_code"]);
                $invoice->set_invoice_id($row["invoice_id"]);
                array_push($invoices, $invoice);
            }
            return $invoices;
        }
    }
    function get_last_invoice()
    {
        $query = "SELECT * FROM invoice ORDER BY invoice_id DESC LIMIT 1";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $row = $result->fetch_assoc();
            $invoice = new Invoice($this->get_user("user_id", $row["user_id"]), $row["transaction_date"], $row["amount"], $row["facturation_addr"], $row["facturation_city"], $row["postal_code"]);
            $invoice->set_invoice_id($row["invoice_id"]);
            return $invoice;
        }
    }
    function remove_invoice($where, $equals)
    {
        $query = "DELETE FROM invoice WHERE $where = '$equals'";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
        } else {
            echo "Invoice ($where = $equals) removed!\n";
        }
    }
    // -------------------------------------------
    // ----------------- STOCK -------------------
    function add_stock($stock)
    {
        $article_id = $stock->article->article_id;
        $query = "INSERT INTO stock (stock_id,article_id,quantity) VALUES (NULL,'$article_id','$stock->quantity')";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error . "\n";
        } else {
            $name = $stock->article->name;
            echo "Stock ($name) added!\n";
        }
    }
    function get_stock($where, $equals)
    {
        $query = "SELECT * FROM stock WHERE $where = '$equals'";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $row = $result->fetch_assoc();
            $stock = new Stock($this->get_article("article_id", $row["article_id"]), $row["quantity"]);
            $stock->set_stock_id($row["stock_id"]);
            return $stock;
        }
    }
    function get_last_stock()
    {
        $query = "SELECT * FROM stock ORDER BY stock_id DESC LIMIT 1";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
            return null;
        } else {
            $result = $this->mysqli->query($query);
            $row = $result->fetch_assoc();
            $stock = new Stock($this->get_article("article_id", $row["article_id"]), $row["quantity"]);
            $stock->set_stock_id($row["stock_id"]);
            return $stock;
        }
    }
    function remove_stock($where, $equals)
    {
        $query = "DELETE FROM stock WHERE $where = '$equals'";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
        } else {
            echo "Stock ($where = $equals) removed!\n";
        }
    }

    function update_stock($base_stock_id, $newStock)
    {
        $article_id = $newStock->article->article_id;
        $query = "UPDATE stock SET article_id = '$article_id', quantity = '$newStock->quantity' WHERE stock_id = '$base_stock_id'";
        $this->mysqli->query($query);
        // error handling
        if ($this->mysqli->errno) {
            echo "Error: " . $this->mysqli->error;
        } else {
            echo "Stock ($base_stock_id) updated!\n";
        }
    }

    function search_article_byName($searchName)
    {
        if ($searchName == "") {
            return $this->get_home_articles();
        } else {
            $query = "SELECT article.article_id, article.name, article.description, article.price, article.publication_date, article.user_id, article.picture_link, user.username FROM article INNER JOIN user ON article.user_id = user.user_id WHERE article.name LIKE '%$searchName%' OR article.description LIKE '%$searchName%' ORDER BY article.publication_date DESC";
            $this->mysqli->query($query);
            // error handling
            if ($this->mysqli->errno) {
                echo "Error: " . $this->mysqli->error;
                return null;
            } else {
                $result = $this->mysqli->query($query);
                $articles = array();
                while ($row = $result->fetch_assoc()) {
                    $article = array();
                    $article["name"] = $row["name"];
                    $article["description"] = $row["description"];
                    $article["price"] = $row["price"];
                    $article["publication_date"] = $row["publication_date"];
                    $article["picture_link"] = $row["picture_link"];
                    $article["username"] = $row["username"];
                    $article["article_id"] = $row["article_id"];
                    array_push($articles, $article);
                }
                return $articles;
            }
        }
    }


// -------------------------------------------
// function UpdateAllUserImage(){
//     // loop 100 times
//     for ($i=1; $i <= 100; $i++) { 
//         $tag = "face";
//         $curl = curl_init();
//         $api_key = "c9ZaQ09f7RgebTnygNQDk9CBpJkiQvEf";
//         curl_setopt($curl, CURLOPT_URL, "https://api.giphy.com/v1/gifs/random?api_key=$api_key&tag=$tag");
//         curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//         $result = curl_exec($curl);
//         curl_close($curl);
//         $result = json_decode($result, true);
//         $link = $result["data"]["images"]["original"]["url"];
//         // download image
//         $image = file_get_contents($link);
//         $image = base64_encode($image);
//         // update user profil_pic
//         $query = "UPDATE user SET profil_pic = '$image' WHERE user_id = '$i'";
//         $this->mysqli->query($query);
//         // error handling
//         if ($this->mysqli->errno) {
//             echo "Error: " . $this->mysqli->error . "\n";
//         } else {
//             echo "User ($i) profil_pic updated!\n";
//         }
//     }






}
?>