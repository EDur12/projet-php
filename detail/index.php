<html>
<head>
  <meta charset="UTF-8">
  <title>LebonCoinCoin</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet"/>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.css" rel="stylesheet"/>
  <link rel="icon" href="./assets/favicon.ico" type="image/svg+xml">
</head>

<body>
<?php
include '../header.php';
session_start();
if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
    $user_id = $user->user_id;
} else {
    $user_id = null;
}
$isValidatePage = false;
$isHomePage = false;
require_once('../fonctions/navBar.php');
require_once('../fonctions/db_manager.php');
$db = new DB_manager("phpmyadmin", "adminphp", "totoduphp", "localhost");
if (isset($_GET['error'])) {
  echo '<div class="alert alert-danger" role="alert"> Not enough stock on this article </div>';
}
$article_id=$_GET['article_id'];
$article = $db->get_article('article_id', $article_id);

?>

<div class="d-flex align-items-center justify-content-center mt-5">
  <div class="card d-flex flex-row mb-3" style="width: 90%; height: 500px;">
    <img src=<?php echo $article->picture_link; ?> class="card-img-left" alt="article_picture" style="width: 50%;">
    <div class="card-body">
      <small class="text-muted d-flex">
      <form action="../redirect.php" method="post">
        <input type="hidden" name="user_id"  value=<?=$article->author->user_id?>>
        <button type="submit" name="see_profil" style="border:none; background: none;"><img src=<?=$article->author->profil_pic?> class="rounded-circle" height="20" width="20" alt="Profil Picture"/></button>
      </form>
      <?= "Published by " . $article->author->username . " on " . $article->publication_date ?> </small>
      <h5 class="card-title mt-5 mb-5"><?php echo $article->name; ?></h5>
      <p class="card-text"><?php echo $article->price; ?> €</p>
      <p class="card-text"  style="height: 150px;>"><?php echo $article->description; ?></p>
      <div class="card-footer d-flex justify-content-between mt-5">
      <a href="../index.php"><i class="fas fa-caret-square-left fa-2x text-warning"></i></a>
      <form method="post" action="../redirect.php" >
      <input type="hidden" name="article_id"  value=<?=$article->article_id?>>
      <input type="hidden" name="user_id"  value=<?=$user_id?>>
      <button type="submit" name="add_article_to_cart" style="border:none; background: none;" ><i class="fas fa-cart-plus fa-2x text-warning"></i></button>
      </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.js"></script>
</body>
</html>
