<html>

<head>
  <meta charset="UTF-8">
  <title>LebonCoinCoin</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.css" rel="stylesheet" />
  <link rel="icon" href="./assets/favicon.ico" type="image/svg+xml">
</head>

<body>
  <?php
  include './header.php';
  session_start();
  if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
  }
  $isHomePage = true;
  require_once('./fonctions/navBar.php');
  ?>
  <div class="row row-cols-1 row-cols-md-3 g-4 p-5">
    <?php
    require_once('./fonctions/db_manager.php');
    $db = new DB_manager("phpmyadmin", "adminphp", "totoduphp", "localhost");
    if (isset($_GET['search'])) {
      $articles = $db->search_article_byName($_GET['search']);
    } else {
      $articles = $db->get_home_articles();
    }

    foreach ($articles as $article):
      $author = $db->get_user('username', $article['username']);
      $articleImage = base64_encode($article['picture_link']);
      $userPP = base64_encode($author->profil_pic);

      ?>
      <div class="col">
        <div class="card h-100">
          <img style="height:200px;"
            src='data:image/jpeg;charset=utf8;base64,<?php echo ($articleImage); ?>'
            class="card-img-top" alt="article_picture" />
          <div class="card-body">
            <h5 class="card-title">
              <?= $article['name'] ?>
            </h5>
            <h7 class="card-title">
              <?= $article["price"] ?> €
            </h7>
          </div>
          <div class="card-footer">
            <small class="text-muted d-flex">
              <form action="../redirect.php" method="post">
                <input type="hidden" name="user_id" value=<?= $author->user_id ?>>
                <button type="submit" name="see_profil" style="border:none; background: none;"><img
                src='data:image/jpeg;charset=utf8;base64,<?php echo ($userPP); ?>' class="rounded-circle border border-warning border-1" height="20"
                    width="20" alt="Profil Picture" /></button>
              </form>
              <?="Published by " . $article["username"] . " on " . $article['publication_date'] ?>
            </small>
            <div class="d-flex justify-content-end">
              <form action="redirect.php" method="post">
                <input type="hidden" id="article_id" name="article_id" value=<?= $article["article_id"] ?>>
                <button type="submit" name="go_to_detail" style="border:none; background: none;"><i
                    class="fas fa-plus-circle fa-1x  text-warning"></i></button>
              </form>
            </div>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.js"> </script>
</body>

</html>