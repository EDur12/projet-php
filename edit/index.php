<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>LebonCoinCoin</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet"/>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.css" rel="stylesheet"/>
  <link rel="icon" href="./assets/favicon.ico" type="image/svg+xml">
</head>

<body>
<?php
include '../header.php';
session_start();
if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
}
$isValidatePage = false;
$isHomePage = false;
require_once('../fonctions/navBar.php');
require_once('../fonctions/db_manager.php');
$db = new DB_manager("phpmyadmin", "adminphp", "totoduphp", "localhost");
$article_id=$_GET['article_id'];
$article = $db->get_article('article_id', $article_id);
$article_picture = base64_encode($article->picture_link);
?>
<form action="../redirect.php" method="post">
    <div class="d-flex align-items-center justify-content-center mt-5">
        <div class="card d-flex flex-row mb-3" style="width: 75%; height: 500px;">
            <img src='data:image/jpeg;charset=utf8;base64,<?php echo ($article_picture); ?>' class="card-img-left" alt="article_picture" style="width: 50%;">
            <div class="card-body">
                <small class="text-muted"><?= "Published by " . $article->author->username . " on " . $article->publication_date ?> </small>
                <input type="text" class="form-control my-3" name='name' placeholder="<?=$article->name?>" style="width:300px"/>
                <input type="text" class="form-control my-3" name='price' placeholder="<?=$article->price . "€"?>" style="width:300px"/>
                <textarea type="text" class="form-control my-3" rows="5" cols="33" name="description"><?=$article->description?></textarea>
                <div class="card-footer text-muted">
                    <div class="d-flex justify-content-between pt-5">
                    <input type="hidden" name="article_id"  value="<?=$article->article_id?>">
                    <input type="hidden" name="user_id" value="<?=$user->user_id?>">
                    <button type="submit" name='save_edit_article' class="btn btn-warning">Save changes</button>
                    <button type="submit" name='cancel_edit_article' class="btn btn-warning">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.js"></script>
</body>
</html>