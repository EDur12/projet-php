<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>LebonCoinCoin</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet"/>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.css" rel="stylesheet"/>
  <link rel="icon" href="./assets/favicon.ico" type="image/svg+xml">
</head>

<body>
<?php
$isValidatePage = false;
$isHomePage = false;
include '../header.php';
session_start();
if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
}
require_once('../fonctions/navBar.php');
require_once('../fonctions/db_manager.php');
$db = new DB_manager("phpmyadmin", "adminphp", "totoduphp", "localhost");
?>
<form action="../redirect.php" method="post">
      <div class="card text-center" style="width: 50%; margin-left: 25%; margin-right: 25%; margin-top: 5%;">
        <div class="card-body">
          <input type="text" class="form-control  mt-1 mb-3" name='article_name' placeholder="Article name" style="width:100%"/>
          <input type="text" class="form-control my-3" name='article_price' placeholder="Article price" style="width:100%"/>
          <textarea type="text" class="form-control my-3" rows="5" cols="0" name="article_description">Article description (max 255 characters)</textarea>
          <input type="text" class="form-control my-3" name='quantity' placeholder="Article quantity (default 1)" style="width:100%"/>
          <input type="file" name="image" class="form-control my-3" style="width:100%">
          <div class="card-footer text-muted">
            <div class="d-flex justify-content-between pt-5">
            <input type="hidden" name="user_id" value="<?=$user->user_id; ?>">
            <input type="submit" name="sell_article" class="btn btn-outline-warning btn-rounded" data-mdb-ripple-color="warning" style="width:100%" value='Add article'>
            </div>
          </div>
        </div>
      </div>
  </div>
</form>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.js"></script>
</body>
</html>