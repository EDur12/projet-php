# Projet PHP

## Database Setup

## Installing `MariaDB`:

- update the package index

    ```bash
    sudo apt update
    ```
- install the package
    ```bash
    sudo apt install mariadb-server
    ```

### Configuring MariaDB:

- Run the security script
    ```bash
    sudo mysql_secure_installation
    ```
    - Switch to unix_socket authentication [Y/n] -> no

    - Change the root password? [Y/n] -> yes

### Test MariaDB:

- Check MariaDb status
    ```bash
    sudo systemctl start mariadb
    sudo systemctl status mariadb
    ```
    ```bash
    [sudo] password for paja444: 
    ● mariadb.service - MariaDB 10.6.11 database server
        Loaded: loaded (/lib/systemd/system/mariadb.service; enabled; vendor preset: enabled)
        Drop-In: /etc/systemd/system/mariadb.service.d
                └─migrated-from-my.cnf-settings.conf
        Active: active (running) since Thu 2023-02-16 15:41:16 CET; 3 days ago
        Docs: man:mariadbd(8)
                https://mariadb.com/kb/en/library/systemd/
    Main PID: 771 (mariadbd)
        Status: "Taking your SQL requests now..."
        Tasks: 9 (limit: 18724)
        Memory: 330.6M
            CPU: 59.510s
        CGroup: /system.slice/mariadb.service
                └─771 /usr/sbin/mariadbd

    ```

### Installing `Apache2`:

- install the apache2 package:
    ```bash
    sudo apt install apache2
    ```
- Adjusting the Firewall
(specific port for this project because Vps 80 port is already used):
    ```bash
    sudo ufw allow '8088'
    ```
- Check apache2 status:
    ```bash
    (base) paja444@Alpagame:~$ sudo systemctl start apache2.service
    (base) paja444@Alpagame:~$ sudo systemctl status apache2.service
    ```
    ```bash
    ● apache2.service - The Apache HTTP Server
        Loaded: loaded (/lib/systemd/system/apache2.service; enabled; vendor preset: enabled)
        Active: active (running) since Sun 2023-02-19 19:24:15 CET; 43s ago
        Docs: https://httpd.apache.org/docs/2.4/
        Process: 168534 ExecStart=/usr/sbin/apachectl start (code=exited, status=0/SUCCESS)
    Main PID: 168538 (apache2)
        Tasks: 6 (limit: 18724)
        Memory: 12.9M
            CPU: 69ms
        CGroup: /system.slice/apache2.service
                ├─168538 /usr/sbin/apache2 -k start
                ├─168539 /usr/sbin/apache2 -k start
                ├─168540 /usr/sbin/apache2 -k start
                ├─168541 /usr/sbin/apache2 -k start
                ├─168542 /usr/sbin/apache2 -k start
                └─168543 /usr/sbin/apache2 -k start
    ```
### Installing phpMyAdmin
- install package:
    ```bash
    cd /tmp
    wget https://files.phpmyadmin.net/phpMyAdmin/5.1.3/phpMyAdmin-5.1.3-all-languages.zip
    ```
- unzip package:
    ```bash
    unzip phpMyAdmin-5.1.3-all-languages.zip
    ```
- move it in `usr/share/phpmyadmin`:
    ```bash
    sudo mv phpMyAdmin-5.1.3-all-languages /usr/share/phpmyadmin
    ```
- make temp directory:
    ```bash
    sudo mkdir -p /var/lib/phpmyadmin/tmp
    ```
- give him correct permissions:
    ```bash
    sudo chown -R www-data:www-data /var/lib/phpmyadmin/
    ```
- get sample:
    ```bash
    cp /usr/share/phpmyadmin/config.sample.inc.php /usr/share/phpmyadmin/config.inc.php
    ```
### phpMyAdmin configuration
- replace `/usr/share/phpmyadmin/config.inc.php
`file by :
    ```bash
    <?php
    /**
    * phpMyAdmin sample configuration, you can use it as base for
    * manual configuration. For easier setup you can use setup/
    *
    * All directives are explained in documentation in the doc/ folder
    * or at <https://docs.phpmyadmin.net/>.
    */

    declare(strict_types=1);

    /**
    * This is needed for cookie based authentication to encrypt password in
    * cookie. Needs to be 32 chars long.
    */
    $cfg['blowfish_secret'] = 'vOch3yX3OM4Bg5aXb7yOh6wPw7ph3QJxKB9lApqsAOw='; /* YOU MUST FILL IN THIS FOR COOKIE AUTH! */

    /**
    * Servers configuration
    */
    $i = 0;

    /**
    * First server
    */
    $i++;
    /* Authentication type */
    $cfg['Servers'][$i]['auth_type'] = 'cookie';
    /* Server parameters */
    $cfg['Servers'][$i]['host'] = 'localhost';
    $cfg['Servers'][$i]['compress'] = false;
    $cfg['Servers'][$i]['AllowNoPassword'] = false;

    /**
    * phpMyAdmin configuration storage settings.
    */

    /* User used to manipulate with storage */
    // $cfg['Servers'][$i]['controlhost'] = '';
    // $cfg['Servers'][$i]['controlport'] = '';
    $cfg['Servers'][$i]['controluser'] = 'adminphp';
    $cfg['Servers'][$i]['controlpass'] = '********';# <-- /!\ set your own passwd

    /* Storage database and tables */
    $cfg['Servers'][$i]['pmadb'] = 'phpmyadmin';
    $cfg['Servers'][$i]['bookmarktable'] = 'pma__bookmark';
    $cfg['Servers'][$i]['relation'] = 'pma__relation';
    $cfg['Servers'][$i]['table_info'] = 'pma__table_info';
    $cfg['Servers'][$i]['table_coords'] = 'pma__table_coords';
    $cfg['Servers'][$i]['pdf_pages'] = 'pma__pdf_pages';
    $cfg['Servers'][$i]['column_info'] = 'pma__column_info';
    $cfg['Servers'][$i]['history'] = 'pma__history';
    $cfg['Servers'][$i]['table_uiprefs'] = 'pma__table_uiprefs';
    $cfg['Servers'][$i]['tracking'] = 'pma__tracking';
    $cfg['Servers'][$i]['userconfig'] = 'pma__userconfig';
    $cfg['Servers'][$i]['recent'] = 'pma__recent';
    $cfg['Servers'][$i]['favorite'] = 'pma__favorite';
    $cfg['Servers'][$i]['users'] = 'pma__users';
    $cfg['Servers'][$i]['usergroups'] = 'pma__usergroups';
    $cfg['Servers'][$i]['navigationhiding'] = 'pma__navigationhiding';
    $cfg['Servers'][$i]['savedsearches'] = 'pma__savedsearches';
    $cfg['Servers'][$i]['central_columns'] = 'pma__central_columns';
    $cfg['Servers'][$i]['designer_settings'] = 'pma__designer_settings';
    $cfg['Servers'][$i]['export_templates'] = 'pma__export_templates';

    /**
    * End of servers configuration
    */

    /**
    * Directories for saving/loading files from server
    */
    $cfg['UploadDir'] = '';
    $cfg['SaveDir'] = '';
    $cfg['TempDir'] = '/var/lib/phpmyadmin/tmp';
    /**
    * Whether to display icons or text or both icons and text in table row
    * action segment. Value can be either of 'icons', 'text' or 'both'.
    * default = 'both'
    */
    //$cfg['RowActionType'] = 'icons';

    /**
    * Defines whether a user should be displayed a "show all (records)"
    * button in browse mode or not.
    * default = false
    */
    //$cfg['ShowAll'] = true;

    /**
    * Number of rows displayed when browsing a result set. If the result
    * set contains more rows, "Previous" and "Next".
    * Possible values: 25, 50, 100, 250, 500
    * default = 25
    */
    //$cfg['MaxRows'] = 50;

    /**
    * Disallow editing of binary fields
    * valid values are:
    *   false    allow editing
    *   'blob'   allow editing except for BLOB fields
    *   'noblob' disallow editing except for BLOB fields
    *   'all'    disallow editing
    * default = 'blob'
    */
    //$cfg['ProtectBinary'] = false;

    /**
    * Default language to use, if not browser-defined or user-defined
    * (you find all languages in the locale folder)
    * uncomment the desired line:
    * default = 'en'
    */
    //$cfg['DefaultLang'] = 'en';
    //$cfg['DefaultLang'] = 'de';

    /**
    * How many columns should be used for table display of a database?
    * (a value larger than 1 results in some information being hidden)
    * default = 1
    */
    //$cfg['PropertiesNumColumns'] = 2;

    /**
    * Set to true if you want DB-based query history.If false, this utilizes
    * JS-routines to display query history (lost by window close)
    *
    * This requires configuration storage enabled, see above.
    * default = false
    */
    //$cfg['QueryHistoryDB'] = true;

    /**
    * When using DB-based query history, how many entries should be kept?
    * default = 25
    */
    //$cfg['QueryHistoryMax'] = 100;

    /**
    * Whether or not to query the user before sending the error report to
    * the phpMyAdmin team when a JavaScript error occurs
    *
    * Available options
    * ('ask' | 'always' | 'never')
    * default = 'ask'
    */
    //$cfg['SendErrorReports'] = 'always';

    /**
    * 'URLQueryEncryption' defines whether phpMyAdmin will encrypt sensitive data from the URL query string.
    * 'URLQueryEncryptionSecretKey' is a 32 bytes long secret key used to encrypt/decrypt the URL query string.
    */
    //$cfg['URLQueryEncryption'] = true;
    //$cfg['URLQueryEncryptionSecretKey'] = '';

    /**
    * You can find more configuration options in the documentation
    * in the doc/ folder or at <https://docs.phpmyadmin.net/>.
    */
    ```
### MariadB configuration for phpMyAdmin:
- create the database by script:
    ```bash
    mysql -u root -p < /usr/share/phpmyadmin/sql/create_tables.sql
    ```
- creat user for manage it:
    ```sql
    GRANT ALL ON *.* TO 'adminphp'@'localhost' IDENTIFIED BY 'password' WITH GRANT OPTION;
    ```
- give him permission for `phpmyadmin`database:
    ```sql
    GRANT ALL PRIVILEGES ON phpmyadmin.* TO 'adminphp'@'localhost' WITH GRANT OPTION;
    ```
- flush:
    ```sql
    FLUSH PRIVILEGES;
    ```
### Apache2 configuration:
- change default site listening port for 8088 at first line:
    ```bash
    nano /etc/apache2//sites-available
    ````
    ```bash
    <VirtualHost *:8088>
    ```

### phpMyAdmin integration in MariaDB:

- Create configuration file for phpMyAdmin:
    ```bash
    sudo nano /etc/apache2/conf-available/phpmyadmin.conf
    ```
    ```bash
    Alias /pma /usr/share/phpmyadmin

    <Directory /usr/share/phpmyadmin>
    Options SymLinksIfOwnerMatch
    DirectoryIndex index.php

    # Disable for distant administration of database (product only)
    #Order deny,allow
    #Deny from all
    #Allow from 192.168.1.0/24

    <IfModule mod_php.c>
        <IfModule mod_mime.c>
        AddType application/x-httpd-php .php
        </IfModule>
        <FilesMatch ".+\.php$">
        SetHandler application/x-httpd-php
        </FilesMatch>

        php_value include_path .
        php_admin_value upload_tmp_dir /var/lib/phpmyadmin/tmp
        
        php_admin_value open_basedir /usr/share/phpmyadmin/:/etc/phpmyadmin/:/var/lib/phpmyadmin/:/usr/share/php/php-gettext/:/usr/share/php/php-php-gettext/:/usr/share/javascript/:/usr/share/php/tcpdf/:/usr/share/doc/phpmyadmin/:/usr/share/php/phpseclib/
        
        php_admin_value mbstring.func_overload 0
    </IfModule>

    </Directory>

    # Deny acces for some folders
    <Directory /usr/share/phpmyadmin/templates>
    Require all denied
    </Directory>
    <Directory /usr/share/phpmyadmin/libraries>
    Require all denied
    </Directory>
    <Directory /usr/share/phpmyadmin/setup/lib>
    Require all denied
    </Directory>
    ```
- save and activate configuration:
    ```bash
    sudo a2enconf phpmyadmin.conf
    ```
- Test:
    ```bash
    sudo apachectl configtest
    ```
    ```bash
    sudo systemctl reload apache2
    ```
- acces by browser at: http://54.37.153.179:8088/pma

- from phpMyAdmin import your database

## Web-Site Setup

### Nginx Install

- Download package:
    ```bash
    sudo apt install nginx
    ```
- update firewall:
    ```bash
    sudo ufw allow 'Nginx HTTP'
    ```
- check status:
    ```bash
    (base) paja444@Alpagame:~$ sudo systemctl status nginx.service 
    ● nginx.service - A high performance web server and a reverse proxy server
        Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)
        Active: active (running) since Fri 2023-02-17 16:29:42 CET; 2 days ago
        Docs: man:nginx(8)
        Process: 63771 ExecStartPre=/usr/sbin/nginx -t -q -g daemon on; master_process on; (code=exited>
        Process: 63772 ExecStart=/usr/sbin/nginx -g daemon on; master_process on; (code=exited, status=>
    Main PID: 63773 (nginx)
        Tasks: 5 (limit: 18724)
        Memory: 6.9M
            CPU: 1.005s
        CGroup: /system.slice/nginx.service
                ├─63773 nginx: master process /usr/sbin/nginx -g daemon on; master_process on;
                ├─63774 nginx: worker process
                ├─63775 nginx: worker process
                ├─63776 nginx: worker process
                └─63777 nginx: worker process
    ```
### Import webSite directory:

- clone git repository:
    ```bash
    (base) paja444@Alpagame:~/B2/info$ git clone https://gitlab.com/EDur12/projet-php.git
    ```
- create a service file `php_project.service`:
    ```bash
    [Unit]
    Description=le bon coincoin php Service
    ConditionPathExists=/home/paja444/B2/info/projet-php
    After=network.target
    [Service]
    Type=simple
    User=paja444
    Group=paja444
    WorkingDirectory=/home/paja444/B2/info/projet-php
    ExecStart=php -S localhost:8808
    Restart=on-failure
    RestartSec=10
    StandardOutput=syslog
    StandardError=syslog
    SyslogIdentifier=leboncoincoin_sevice
    [Install]
    WantedBy=multi-user.target
    ```
- reload systemctl daemon:
    ```bash
    (base) paja444@Alpagame:~$ sudo systemctl daemon-reload
    ```
- start web service and check his status:
    ```bash
    (base) paja444@Alpagame:~$ sudo systemctl start php_project.service
    (base) paja444@Alpagame:~$ sudo systemctl status php_project.service 
    ● php_project.service - le bon coincoin php Service
        Loaded: loaded (/etc/systemd/system/php_project.service; disabled; vendor preset: enabled)
        Active: active (running) since Fri 2023-02-17 16:34:00 CET; 2 days ago
    Main PID: 64142 (php)
        Tasks: 1 (limit: 18724)
        Memory: 9.8M
            CPU: 6.648s
        CGroup: /system.slice/php_project.service
                └─64142 php -S localhost:8808
    ```

- Create nginx website configuration file `php-project.conf`:
    ```bash
    (base) paja444@Alpagame:/etc/nginx/conf.d$ sudo nano php-project.conf 
    ```
- add the folowing content:
    ```bash
    upstream backend_php{
    ip_hash;
    server localhost:8808;
    }

    limit_req_zone $binary_remote_addr zone=mylimit:10m rate=10r/s;

    server {
        listen 80 default_server;
        server_name _;
        return 301 https://alpagam.com$request_uri;
    }
    server {
    listen 443 ssl;
    ssl_certificate /etc/letsencrypt/live/alpagam.com-0001/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/alpagam.com-0001/privkey.pem;
    server_name www.alpagam.com alpagam.com;

        location / {
                proxy_pass http://backend_php;
        }
    }
    ```
- restart nginx:
    ```bash
    (base) paja444@Alpagame:/etc/nginx/conf.d$ sudo systemctl restart nginx.service
    ```

## Usage:

Go to https://alpagam.com and enjoy the market.