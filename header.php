<?php
require_once('fonctions/db_manager.php');
$db = new DB_manager("phpmyadmin", "adminphp", "totoduphp", "localhost");

if (isset($_POST['register'])) {
    if (!empty($_FILES["image"]["name"])) {
        // Get file info 
        $fileName = basename($_FILES["image"]["name"]);
        $fileType = pathinfo($fileName, PATHINFO_EXTENSION);
        // Allow certain file formats 
        $allowTypes = array('jpg', 'png', 'jpeg', 'gif');
        if (in_array($fileType, $allowTypes)) {
            $image = $_FILES['image']['tmp_name'];
            $imgContent = addslashes(file_get_contents($image));
            $hashPassword = password_hash($_POST['password'], PASSWORD_BCRYPT);
            $user = new User($_POST['username'], $hashPassword, $_POST['mail'], 'user', $imgContent);
            $db->add_user($user);
            session_start();
            $_SESSION['user'] = $user;
            header("Location: /");
        } else {
            // error message
        }
    }
}

if (isset($_POST['login'])) {
    $user = $db->get_user('username', $_POST['username']);
    if (password_verify($_POST['password'], $user->password)) {
        session_start();
        $_SESSION['user'] = $user;
        header("Location: /");
    } else {
        header("Location: /login?error=1");
    }
}

if (isset($_POST['logout'])) {
    session_start();
    session_destroy();
    header("Location: /");
}

?>